<?php
/**
 * Created by PhpStorm.
 * User: pomazkinis
 * Date: 19.10.2018
 * Time: 10:33
 */

namespace app\commands;

use app\components\Helper;
use Yii;
use yii\console\Controller;
use app\models\User;
use yii\db\Query;

class InitController extends Controller
{

    public function actionAddUser()
    {
        print 'input email: ';
        $login = trim(fgets(STDIN));
        while(true)
        {
            print 'input password: ';
            $pass1 = trim(fgets(STDIN));
            print 'confirm password: ';
            $pass2 = trim(fgets(STDIN));
            if ($pass1==$pass2)
                break;
            else
            {
                print 'Error: password differents!'.PHP_EOL;
            }
        }

        $user = new User();
        $user->setPassword($pass1);
        $user->email = $login;
        $user->username = $login;
        print PHP_EOL;
        if ($user->save(false))
        {
            print 'user succesfully created!';
        }
        else{
            print 'An errors occured'.PHP_EOL;
            foreach($user->errors as $e)
            {
                print $e.PHP_EOL;
            }
        }



    }



}
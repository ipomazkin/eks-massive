<?php

use yii\db\Migration;

/**
 * Class m190819_074943_settings
 */
class m190819_074943_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // echo `php yii migrate --migrationPath=@yii/rbac/migrations/`;
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('settings',[
            'id'=>$this->primaryKey(11),


            'title'=>$this->string(255),
            'kwds'=>$this->string(255),
            'description'=>$this->string(255),

            'country'=>$this->string(255),
            'city'=>$this->string(255),
            'region'=>$this->string(255),
            'street'=>$this->string(255),
            'building'=>$this->string(255),
            'office'=>$this->string(255),
            'postcode'=>$this->string(255),
            'phone'=>$this->string(255),
            'email'=>$this->string(255),
            'soc_vk'=>$this->string(255),
            'soc_ok'=>$this->string(255),
            'soc_google'=>$this->string(255),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190819_074943_settings cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m190819_070235_sections
 */
class m190819_070235_sections extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // echo `php yii migrate --migrationPath=@yii/rbac/migrations/`;
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('sections',[
            'id'=>$this->primaryKey(11),
            'active'=>$this->integer(1)->defaultValue(1),
            'title'=>$this->string(255),
            'subtitle'=>$this->string(255),
            'text'=>$this->text(),

            //===== images ===================//
            'image1_src'=>$this->string(255),
            'image1_title'=>$this->string(255),
            'image1_description'=>$this->string(255),

            'image2_src'=>$this->string(255),
            'image2_title'=>$this->string(255),
            'image2_description'=>$this->string(255),

            'image3_src'=>$this->string(255),
            'image3_title'=>$this->string(255),
            'image3_description'=>$this->string(255),

            'image4_src'=>$this->string(255),
            'image4_title'=>$this->string(255),
            'image4_description'=>$this->string(255),

            'image5_src'=>$this->string(255),
            'image5_title'=>$this->string(255),
            'image5_description'=>$this->string(255),

            'image6_src'=>$this->string(255),
            'image6_title'=>$this->string(255),
            'image6_description'=>$this->string(255),

            //===========   properties ===================//
            'property1_title'=>$this->string(255),
            'property1_description'=>$this->string(255),

            'property2_title'=>$this->string(255),
            'property2_description'=>$this->string(255),

            'property3_title'=>$this->string(255),
            'property3_description'=>$this->string(255),

            'property4_title'=>$this->string(255),
            'property4_description'=>$this->string(255),

            'created_at'=>$this->integer(11),
            'updated_at'=>$this->integer(11),

        ], $tableOptions);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropTable('sections');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190819_070235_sections cannot be reverted.\n";

        return false;
    }
    */
}

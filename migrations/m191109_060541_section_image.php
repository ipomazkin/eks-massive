<?php

use yii\db\Migration;

/**
 * Class m191109_060541_section_image
 */
class m191109_060541_section_image extends Migration
{
    public function safeUp()
    {
        // echo `php yii migrate --migrationPath=@yii/rbac/migrations/`;
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('section_image',[
            'id'=>$this->primaryKey(11),
            'section_id'=>$this->integer(11),
            'image'=>$this->string(255),
            'is_logo'=>$this->integer(1)->defaultValue(0),
        ], $tableOptions);

        $this->addForeignKey('section_image','section_image','section_id','sections','id','cascade','cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('section_image','section_image');
        $this->dropTable('section_image');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191109_060541_section_image cannot be reverted.\n";

        return false;
    }
    */
}

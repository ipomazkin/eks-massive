<?php
/**
 * Created by PhpStorm.
 * User: pomazkinis
 * Date: 18.08.2019
 * Time: 17:52
 */
return [
    ''=>'site/index',
    'admin'=>'admin/index',
    '<controller:[\w]+>/<action:[0-9-_\w]+>' => '<controller>/<action>',
];
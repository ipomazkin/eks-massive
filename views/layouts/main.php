<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\LandingAsset;
use yii\helpers\Url;
use app\components\Helper;

LandingAsset::register($this);

$settings = Yii::$app->params['settings'];
/* @var $settings \app\models\Settings*/

$section4 = $section2 = $section3 = $section1= null;

if (isset(Yii::$app->params['sections1'])  && Yii::$app->params['sections1']->active)
    $section1 = Yii::$app->params['sections1'];
if (isset(Yii::$app->params['sections2'])  && Yii::$app->params['sections2']->active)
    $section2 = Yii::$app->params['sections2'];
if (isset(Yii::$app->params['sections3'])  && Yii::$app->params['sections3']->active)
    $section3 = Yii::$app->params['sections3'];
if (isset(Yii::$app->params['sections4'])  && Yii::$app->params['sections4']->active)
    $section4 = Yii::$app->params['sections4'];

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<header id="header">
    <nav class="navbar navbar-default navbar-static-top" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Отобразить навигацию</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand">
                    <a href="/"><h1><?=$settings->title?></h1></a>
                </div>
            </div>
            <div class="navbar-collapse collapse">
                <div class="menu">
                    <ul class="nav nav-tabs" role="tablist">
                         <?php if($section2):?>
                            <li role="presentation"><a href="#section2" class=""><?=$section2->title?></a></li>
                        <?php endif?>
                        <?php if($section3):?>
                            <li role="presentation"><a href="#section3" class=""><?=$section3->title?></a></li>
                        <?php endif?>
                        <?php if($section4):?>
                            <li role="presentation"><a href="#section4" class=""><?=$section4->title?></a></li>
                        <?php endif?>
                        <?php if($section1 || $section2 || $section3 || $section4):?>
                            <li role="presentation"><a href="#contacts">Контакты</a></li>
                        <?php else:?>
                            <li role="presentation"><a href="/">На главную</a></li>
                        <?php endif;?>
                    </ul>
                </div>
            </div>
        </div>
        <!--/.container-->
    </nav>
    <!--/nav-->
</header>
<!--/header-->
<?= $content ?>

<?php
$js=<<<HERE
  wow = new WOW({}).init();
HERE;

$this->registerJs($js);
?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
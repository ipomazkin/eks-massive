<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */
/* @var $form ActiveForm */
?>
<div class="admin-settings">
    <h1>Обшие Настройки</h1>
    <?php $form = ActiveForm::begin(); ?>
        <div class="col-md-8">
            <?= $form->field($model, 'title')->label('Название лендинга (Тег title)') ?>
            <?= $form->field($model, 'kwds')->label('Мета тег keywords') ?>
            <?= $form->field($model, 'description')->label('Мета тег description') ?>
            <?= $form->field($model, 'country')->label('Страна') ?>
            <?= $form->field($model, 'city')->label('Город')  ?>
            <?= $form->field($model, 'region')->label('Регион')  ?>
            <?= $form->field($model, 'street')->label('Улица')  ?>
            <?= $form->field($model, 'building')->label('Дом') ?>
            <?= $form->field($model, 'office')->label('офис') ?>
            <?= $form->field($model, 'postcode')->label('почтовый индекс') ?>
            <?= $form->field($model, 'phone')->label('Телефон') ?>
            <?= $form->field($model, 'email')->label('Email') ?>
            <?= $form->field($model, 'soc_vk') ->label('В контакте (ссылка)')?>
            <?= $form->field($model, 'soc_ok')->label('Одноклассники (ссылка)') ?>
            <?= $form->field($model, 'soc_google')->label('Google+ (ссылка)') ?>
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>


    

    <?php ActiveForm::end(); ?>

</div><!-- admin-settings -->

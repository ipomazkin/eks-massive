<?php
/**
 * Created by PhpStorm.
 * User: pomazkinis
 * Date: 19.08.2019
 * Time: 12:57
 *
 * @var $this \yii\web\View
 * @var $model \app\models\Sections
 *
 * Секция 1. Редактируем фото на баннере, title и subtitle
 *
 */
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\widgets\FileInput;
use app\components\Helper;
list($initialPreview, $initialPreviewConfig) = $model->initialPreview;

?>
<div  class="col-md-12 col-lg-12 review-form">
    <h1>Секция 2</h1>
    <div class="">
        <?php $form = ActiveForm::begin([
            'id' => 'section',
            'enableClientValidation' => true,
            'options' => [
                'class' => '',
                'accept-charset' => 'UTF-8'
            ]
        ])
        ?>

        <?= $form->field($model,'active')->dropDownList(['0'=>'Нет',1=>'Да'])?>


        <?=$form->field($model,'title')->textInput()?>

        <?=$form->field($model,'subtitle')->textInput()?>


        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>

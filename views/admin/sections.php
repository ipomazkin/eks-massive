<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sections */
/* @var $form ActiveForm */
?>
<div class="admin-sections">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'active') ?>
        <?= $form->field($model, 'created_at') ?>
        <?= $form->field($model, 'updated_at') ?>
        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'subtitle') ?>
        <?= $form->field($model, 'image1_src') ?>
        <?= $form->field($model, 'image1_title') ?>
        <?= $form->field($model, 'image1_description') ?>
        <?= $form->field($model, 'image2_src') ?>
        <?= $form->field($model, 'image2_title') ?>
        <?= $form->field($model, 'image2_description') ?>
        <?= $form->field($model, 'image3_src') ?>
        <?= $form->field($model, 'image3_title') ?>
        <?= $form->field($model, 'image3_description') ?>
        <?= $form->field($model, 'image4_src') ?>
        <?= $form->field($model, 'image4_title') ?>
        <?= $form->field($model, 'image4_description') ?>
        <?= $form->field($model, 'image5_src') ?>
        <?= $form->field($model, 'image5_title') ?>
        <?= $form->field($model, 'image5_description') ?>
        <?= $form->field($model, 'image6_src') ?>
        <?= $form->field($model, 'image6_title') ?>
        <?= $form->field($model, 'image6_description') ?>
        <?= $form->field($model, 'property1_title') ?>
        <?= $form->field($model, 'property1_description') ?>
        <?= $form->field($model, 'property2_title') ?>
        <?= $form->field($model, 'property2_description') ?>
        <?= $form->field($model, 'property3_title') ?>
        <?= $form->field($model, 'property3_description') ?>
        <?= $form->field($model, 'property4_title') ?>
        <?= $form->field($model, 'property4_description') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- admin-sections -->

<?php
/**
 * Created by PhpStorm.
 * User: pomazkinis
 * Date: 19.08.2019
 * Time: 15:22
 */

use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="container">
    <?=Html::tag('h1','Админка');?>

    <div class="col-md-4 col-xs-12">
        <?=Html::tag('h2','Общее')?>
        <?=Html::a('Настройки ',Url::to(['admin/settings']))?>
    </div>

    <div class="col-md-4 col-xs-12">
        <?=Html::tag('h2','Секции')?>
        <?php foreach(range(1,4) as $key):?>
            <div>
                <?=Html::a('Секция '.$key,Url::to(['admin/sections','section_id'=>$key]))?>
            </div>
        <?php endforeach;?>
    </div>

</div>

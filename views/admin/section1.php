<?php
/**
 * Created by PhpStorm.
 * User: pomazkinis
 * Date: 19.08.2019
 * Time: 12:57
 *
 * @var $this \yii\web\View
 * @var $model \app\models\Sections
 *
 * Секция 1. Редактируем фото на баннере, title и subtitle
 *
 */
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\widgets\FileInput;
use app\components\Helper;
list($initialPreview, $initialPreviewConfig) = $model->initialPreview;

?>
<div class="col-md-12 col-lg-12 review-form">
    <h1>Секция 1 (большой баннер на главной)</h1>
    <div class="">
        <?php $form=ActiveForm::begin([
            'id'=>'input-form',
            'class'=>'',
            'options'=>[
                'enctype' => 'multipart/form-data'
            ],
        ]);?>

        <?= $form->field($model,'active')->dropDownList(['0'=>'Нет',1=>'Да'])?>

        <?= FileInput::widget([
            'name' => 'Sections[images][]',
            'options'=>[
                'multiple'=>true
            ],
            'pluginOptions' => [
                'previewFileType' => 'any',
                'maxFileCount' => 100,
                'maxFileSize'=>10485760,
            ]
        ]);?>

        <?php if($model->hasUploadedImages()):?>
            <h5>Загруженные изображения</h5>
            <div class="uploaded_photos col-md-12">
                <?php foreach($model->hasUploadedImages as $firmImage):?>
                    <?php
                    /* @var $firmImage \app\models\SectionImage*/
                    ?>
                    <div style="width:220px;height:300px" class="parent-block-uploaded-file file-preview-frame krajee-default  kv-preview-thumb col-md-1">
                        <div style="height:200px;overflow: hidden" class="kv-file-content">
                            <?=Html::img(Yii::getAlias('@images_web/') .$firmImage->image,['width'=>200,'height'=>180])?>
                        </div>
                        <div class="file-thumbnail-footer">
                            <div class="file-footer-caption"><?=$firmImage->image?></div>
                            <div class="file-actions clear">
                                <button class="btn btn-danger delete-old-image">-</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="can-be-removed-block">
                            <?=Html::input('hidden','Sections[hasUploadedImages][]',$firmImage->image)?>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        <?php endif;?>
        <?=Html::error($model, 'hasUploadedImages', ['class'=>'help-block'])?>


        <div class="clearfix"></div>

        <?php foreach (range(1,6) as $v):?>
            <div class="col-md-6 col-lg-6">
                <?=$form->field($model,'image'.$v.'_title')->label('Заголовок к баннеру №'.$v)->textInput()?>
            </div>
            <div class="col-md-6 col-lg-6">
                <?=$form->field($model,'image'.$v.'_description')->label('Описание к баннеру №'.$v)->textInput()?>
            </div>
        <?php endforeach;?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>

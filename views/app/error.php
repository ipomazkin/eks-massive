<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <div class="container">
        <h1>Страница не найдена</h1>
        <?=Html::a('На главную', '/')?>
    </div>




</div>

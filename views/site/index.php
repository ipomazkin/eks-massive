<?php

/**
 * @var $this \yii\web\View
 * @var $settings \app\models\Settings
 * @var $section1 \app\models\Sections
 * @var $section2 \app\models\Sections
 * @var $section3 \app\models\Sections
 * @var $section4 \app\models\Sections
 */

use yii\helpers\Url;
use yii\helpers\Html;
use app\components\Helper;

$js=<<<HERE
$(function(){
        $("a[href^='#']").click(function(){
                event.preventDefault;
                var href = $(this).attr("href");
                $("html, body").animate({scrollTop: $(href).offset().top+"px"});
        });
});
HERE;

$this->registerJs($js);

//SEO
$this->title = $settings->title;
//keywords
$this->registerMetaTag([
    'name'=>'keywords',
    'content'=>$settings->kwds
]);
//description
$this->registerMetaTag([
    'name'=>'description',
    'content'=>$settings->description
]);
?>
<?php foreach(range(1,4) as $v):?>
    <?php if(${'section'.$v}->active):?>
        <?php echo $output = $this->render('section'.$v,compact('section'.$v))?>
    <?php endif;?>
<?php endforeach;?>

<?=$this->render('footer',compact('settings'))?>

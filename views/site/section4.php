<?php
/**
 * Created by PhpStorm.
 * User: pomazkinis
 * Date: 19.08.2019
 * Time: 16:44
 */

/* @var $section4 \app\models\Sections*/
/* @var $this \yii\web\View*/

use app\components\Helper;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div id="section4" class="gallery">
    <div class="text-center">
        <h2><?=$section4->title?></h2>
        <p><?=$section4->subtitle?></p>
    </div>

    <?php if($section4->hasUploadedImages()):?>
        <?php foreach(array_chunk($section4->hasUploadedImages, 3) as $k=>$group):?>
            <?=$this->render('container', [
                    'images'=>$group,
                    'key'=>$k,
                    'section4'=>$section4
            ])?>
        <?php endforeach;?>
    <?php endif?>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: pomazkinis
 * Date: 19.08.2019
 * Time: 16:43
 */
/* @var $section3 \app\models\Sections*/
/* @var $this \yii\web\View*/

use app\components\Helper;
use yii\helpers\Html;
use yii\helpers\Url;
//Helper::debug($section3);
?>

<div id="section3" class="services">
    <div class="container">
        <div class="text-center">
            <h2><?=$section3->text?></h2>
        </div>
    </div>
</div>
<section class="action">
    <div class="container">
        <div class="left-text hidden-xs">
            <h4><?=$section3->image1_title?></h4>
            <p><?=$section3->image1_description?></p>
        </div>
        <div class="right-image hidden-xs" style="background-image: url(<?=Yii::getAlias('@images_web/'.$section3->sectionImages[0]->image ?? null)?>)"></div>
    </div>
</section>

9049894834

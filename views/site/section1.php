<?php
/**
 * Created by PhpStorm.
 * User: pomazkinis
 * Date: 19.08.2019
 * Time: 16:38
 *
 * @var $section1 \app\models\Sections
 * @var $this \yii\web\View
 */

use app\components\Helper;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="slider">
    <div id="about-slider">
        <div id="carousel-slider" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->

            <ol class="carousel-indicators visible-xs">
                <?php if ($section1->hasUploadedImages()):?>
                <?php foreach($section1->hasUploadedImages as $k=>$sectionImage):?>
                        <?php
                    /* @var $sectionImage \app\models\SectionImage*/
                        ?>
                        <?=Html::tag('li','',[
                            'data-target'=>'#carousel-slider',
                            'data-slide-to'=>$k,
                            'class'=>$k === 0 ? 'active':''
                        ])?>
                <?php endforeach;?>
                <?php endif;?>
            </ol>

            <div class="carousel-inner">
                <?php foreach($section1->hasUploadedImages as $k=>$sectionImage):?>
                    <div class="item <?=(!$k ? 'active': '')?>">
                        <img src="<?=Yii::getAlias('@images_web/'.$sectionImage->image)?>" class="img-responsive" alt="">
                        <div class="carousel-caption">
                            <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.3s">
                                <h2><span><?=$section1->images_titles[$k]?></span></h2>
                            </div>
                            <div class="col-md-10 col-md-offset-1">
                                <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.6s">
                                    <p style="background: none"><?=$section1->images_descriptions[$k]?></p>
                                </div>
                            </div>
                            <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.9s"> </div>
                        </div>
                    </div>
                <?php endforeach;?>


            <a class="left carousel-control hidden-xs" href="#carousel-slider" data-slide="prev">
                <i class="fa fa-angle-left"></i>
            </a>

            <a class=" right carousel-control hidden-xs" href="#carousel-slider" data-slide="next">
                <i class="fa fa-angle-right"></i>
            </a>
        </div>
        <!--/#carousel-slider-->
    </div>
    <!--/#about-slider-->
</div>
<!--/#slider-->

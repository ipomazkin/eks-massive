<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\ContactForm
 */

use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

?>
<? $form=ActiveForm::begin()?>

<?=$form->field($model,'name')->label('Как к Вам обращаться?')->textInput(['id'=>'mask'])?>
     <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
      'mask' => '(999) 999-9999',
  ]) ?>

<?php ActiveForm::end()?>
<?
//$this->registerJs('$("#contactform-phone").inputmask({"mask": "(999) 999-9999"});');

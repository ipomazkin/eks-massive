<?php
/* @var $section2 \app\models\Sections*/
/* @var $this \yii\web\View*/

use app\components\Helper;
use yii\helpers\Html;
use yii\helpers\Url;
use johnitvn\ajaxcrud\CrudAsset;


CrudAsset::register($this);
?>
<div id="section2" class="about">
    <div class="container">
        <div class="text-center">
            <h2><?=$section2->subtitle?></h2>
            <div class="col-md-10 col-md-offset-1">
                <p><?//=$section2->subtitle?></p>
            </div>
        </div>

        <div style="display: flex; justify-content: center;align-items: center; width:100%">
            <div class="btn-gamp">
                <?=Html::a('Заказать звонок',Url::to(['site/call']),[
                        'title'=>'Заказать звонок',
                        'class'=>'call-button',
                        'type'=>'submit',
                        'role'=>'modal-remote'
                ])?>
            </div>
        </div>
    </div>
</div>
<hr>
<?php
/**
 * Created by PhpStorm.
 * User: pomazkinis
 * Date: 19.08.2019
 * Time: 16:44
 *
 * @var $settings \app\models\Settings
 * @var $section4 \app\models\Sections | null
 */

use app\components\Helper;
use yii\bootstrap\Modal;
$section4  =Yii::$app->params['sections4'];
?>
<footer id="contacts">
    <div class="container">
        <div class="col-md-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
            <h4>Контакты</h4>
            <div class="contact-info">
                <ul>
                    <li><i class="fa fa-home fa"></i><?=$settings->postcode?>, <?=$settings->region?>, <?=$settings->city?></li>
                    <li><i class="fa fa-phone fa"></i> <?=$settings->phone ?></li>
                    <li><i class="fa fa-envelope fa"></i> <?=$settings->email?> </li>
                </ul>
            </div>
        </div>

        <?php if($section4 instanceof \app\models\Sections && $section4->active):?>
        <div class="col-md-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="text-center">

                <h4><?=$section4->title?></h4>
                <?php if($section4->hasUploadedImages()):?>
                    <ul class="sidebar-gallery">
                        <? foreach ($section4->hasUploadedImages as $sectionImage):?>
                            <li>
                                <a href="<?=Yii::getAlias('@images_web/'.$sectionImage->image)?>" rel="prettyPhoto">
                                    <img  src="<?=Yii::getAlias('@images_web/'.$sectionImage->image)?>" alt="" class="img-responsive" />
                                </a>
                            </li>
                        <?php endforeach;?>
                    </ul>
                <?php endif;?>
            </div>
        </div>
        <?php endif;?>

        <div class="col-md-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms">
            <div class="">
                <h4>Обратная связь</h4>
                <div class="btn-gamp">
                    <a type="submit" role="modal-remote" href="<?=\yii\helpers\Url::to(['site/call'])?>" class="btn btn-default">Заказать звонок</a>
                </div>
            </div>
        </div>

    </div>
</footer>

<div class="sub-footer">
    <div class="container">
        <div class="social-icon">
            <div class="col-md-4">
                <ul class="social-network">
                    <?php if($settings->soc_google):?>
                    <li><a href="<?=$settings->soc_google?>" class="gplus tool-tip" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
                    <?php endif;?>
                    <?php if($settings->soc_vk):?>
                    <li><a href="<?=$settings->soc_vk?>" class="vkontakte tool-tip" title="vk.com"><i class="fa fa-instagram"></i></a></li>
                    <?php endif;?>
                    <?php if($settings->soc_ok):?>
                    <li><a href="<?=$settings->soc_ok?>" class="ytube tool-tip" title="ok.ru"><i class=""></i></a></li>
                    <?php endif;?>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
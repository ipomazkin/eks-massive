<?php
use app\models\SectionImage;
use app\components\Helper;

/**
 * @var $section4 \app\models\Sections
 * @var SectionImage[]  $images array
 * @var $key int
 */

?>

<div class="container">
    <?php foreach($images as $k=>$sectionImage):?>
            <div class="col-md-4">
                <figure class="effect-marley">
                    <a href="<?=Yii::getAlias('@images_web/'.$sectionImage->image)?>" rel="prettyPhoto">
                    <img src="<?=Yii::getAlias('@images_web/'.$sectionImage->image)?>" alt="" class="img-responsive" />
                    </a>
                    <figcaption>
                        <?php
                        $kk = $key*3 + $k;
                        ?>
                        <?php if(isset($section4->images_titles[$kk])):?>
                        <h4><?=$section4->images_titles[$kk]?></h4>
                        <p><?=$section4->images_descriptions[$kk]?></p>
                        <?php endif;?>
                    </figcaption>
                </figure>
            </div>
    <?php endforeach;?>
</div>
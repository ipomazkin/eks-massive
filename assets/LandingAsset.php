<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LandingAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/css/font-awesome.css',
        '/css/animate.css',
        '/css/style.css',
        '/prettyphoto-master/css/prettyPhoto.css',
    ];
    public $js = [
      //  '/js/jquery.min.js',
      //  '/js/jquery-migrate.min.js',
       // '/js/bootstrap.js',
        '/js/wow.min.js',
        '/prettyphoto-master/js/jquery.prettyPhoto.min.js',
        '/js/script.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}

<?php
/**
 * Created by PhpStorm.
 * User: Home
 * Date: 13.07.2019
 * Time: 18:53
 */

namespace app\components;

use yii\base\BaseObject;
use yii\db\ActiveQuery;
use yii\db\Query;

/**
 * Class QueryIterator
 * @package common\components
 *
 *
 * the iterator helps to walk throw big data sql result
 *
 *         $query = Firms::find()
                ->where('url is null');

            $queryIterator = new QueryIterator([
                'query'=>$query,
                'limit'=>10000
            ]);

            while(true)
            {
                $firms = $queryIterator->iterate();

               //do something..
             if ($queryIterator->isEnd())
                        break;
 *          }
 *
 *
 *
 */
class QueryIterator extends BaseObject
{

    /* @var Query|ActiveQuery  the query to be executed*/
    public $query;

    /* @var int The count fetched rows*/
    public $limit;

    /* @var int To start from*/
    protected $offset=0;

    /* @var int */
    protected $current_iteration=0;
    /**
     * @var array
     */
    protected $result;
    /* @var int*/
    protected $founded_count;

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function iterate()
    {
        $this->founded_count = null;
        $this->offset = $this->limit * $this->current_iteration;
        $this->result = $this->query
            ->offset($this->offset)
            ->limit($this->limit)
            ->all();

        $this->current_iteration++;
        return $this->result;

    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return int
     */
    public function getFoundedCount()
    {
        $this->founded_count = count($this->result);
        return $this->founded_count;
    }

    /**
     * @return bool
     */
    public function isEnd()
    {
        return $this->founded_count === null ? !($this->getFoundedCount()) : !$this->founded_count;
    }

    /**
     * @return int
     */
    public function  getOffset()
    {
        return $this->offset;
    }



}
<?php
/**
 * Created by PhpStorm.
 * User: pomazkinis
 * Date: 03.07.2018
 * Time: 11:41
 */


namespace app\components;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\VarDumper;

class Helper
{

    protected static $time=null;

    const LANG_LOWERCASE=0;

    protected static $timer;
    protected static $timerName;


    const YA_MAP_URI='https://static-maps.yandex.ru/1.x/';


    /**
     * @param $lon string Широта
     * @param $lat string Долгота
     * @param int $zoom
     * @param string $size
     *
     * @return string Url image
     *
     */
    public static function YandexMap($lon,$lat,$zoom=16,$size='180,160',$icon='pm2ntm')
    {
        //?ll='.$firm['lon'].','.$firm['lat'] .'&z=16&l=map&size=180,160&pt='.$firm['lon'].','.$firm['lat'].',pm2rdm';
        $location = $lon.','.$lat;
        $query_params=[
            'll'=>$location,
            'z'=>$zoom,
            'l'=>'map',
            'size'=>$size,
            'pt'=>$location,

        ];

        return self::YA_MAP_URI.'?'.http_build_query($query_params).','.$icon;
    }


    public static function debug($arr,$die=true)
    {
        print '<pre>';
        print_r($arr);
        print '</pre>';
        $die && die;
    }

    /**
     * @param $name
     */
    public static function timerStart($name)
    {
        self::$timer = microtime();
        self::$timerName = $name;
    }

    /**
     *
     */
    public static function timerEnd()
    {
        $current = microtime();
        $diff = $current - self::$timer;
        print self::$timerName.' ...  '.$diff.PHP_EOL;
    }


    /**
     * @param $str
     * @return null|string|string[]
     *
     * Удаляет все спецсиволы из строки
     */
    public static function clearArticle($str)
    {
        $str = preg_replace('/\D/','',$str);
        $str = self::prepareBrand($str);
        return $str;
    }

    /**
     * @param $v
     * @param $roundInt bool
     * @return float|int
     */
    public static function invertPercent($v,$roundInt=false)
    {
        $p =  100 - floatval($v);
        return $roundInt ? round($p) : $p;
    }


    /**
     * @param $str
     * @return string
     *
     * Чистим бренд и приводим его к нижему регистру
     */
    public static function prepareBrand($str)
    {
        $str = trim(mb_strtolower($str));
        return $str;
    }


    public static function dump($str,$die=true)
    {
        VarDumper::dump($str,10,true);
        $die && die;
    }


    /**
     * @param $p1
     * @param $p2
     * @return bool
     *
     * СРАВНИВАЕТ две строки с учетом кириллицы
     * актуально для сравнения брендов
     */
    public static function compareNoCaseSensitive($p1,$p2)
    {
        //переводим в верхний регистр
        //с учетом кириллицы
        $p1 = trim(mb_strtoupper($p1));
        $p2 = trim(mb_strtoupper($p2));
        return $p1==$p2;

    }

    public static function mb_ucfirst($str, $enc = 'utf-8') {
        return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc).mb_substr($str, 1, mb_strlen($str, $enc), $enc);
    }


    /**
     * @param $string
     * @param int $case
     * @return mixed|null|string|string[]
     */
    public static function toEngUrl($string, $case = Helper::LANG_LOWERCASE)
    {
        $rus = array('А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я',
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я');
        if ($case == Helper::LANG_LOWERCASE)
            $lat = array('a','b','v','g','d','e','e','j','z','i','i','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sch','y','y','y','e','yu','ya',
                'a','b','v','g','d','e','e','j','z','i','y','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sch','y','y','y','e','yu','ya');
        else
            $lat = array('A','B','V','G','D','E','E','J','Z','I','Y','K','L','M','N','O','P','R','S','T','U','F','H','C','Ch','Sh','Sch','Y','Y','Y','E','Yu','Ya',
                'a','b','v','g','d','e','e','j','z','i','y','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sch','y','y','y','e','yu','ya');
        $translate = str_replace($rus, $lat, $string);
        $translate = preg_replace('/[^A-Za-z0-9]/', '-', $translate);
        $translate = preg_replace('/-+/', '-', $translate);
        $translate = trim($translate, '-');
        if ($case==Helper::LANG_LOWERCASE) {
            $translate = mb_strtolower($translate);
        }

        return $translate;
    }

    public static function toRusUrl($string, $case = Helper::LANG_LOWERCASE)
    {
        $translate = mb_strtolower($string);
        $translate = preg_replace('/[^а-яёa-z0-9]/u', '-', $translate);
        $translate = preg_replace('/-+/u', '-', $translate);
        $translate = trim($translate, '-');

        return $translate;
    }

    public static function toUrl($string, $case = Helper::LANG_LOWERCASE){
        return self::toEngUrl($string, $case);
    }


    /**
     * @param $string
     * @param $array
     * @return bool
     *
     * сравнивает строку с каждым элементом массива
     * регистр не важен.
     */
    public static function compareStringArray($string,array $array)
    {
        foreach($array as $item)
        {
            if (self::compareNoCaseSensitive($string,$item))
                return true;
        }
        return false;
    }





    /**
     * @param $str
     * @return mixed
     */
    public static function replaceSlashes($str)
    {
        $str = str_replace(['/','\\'],'',$str);
        return $str;
    }



    /**
     * @param $arr
     * @param bool $die
     */
    public static function safeDebug($arr,$die=false)
    {
        if (isset($_COOKIE['__DBG__']))
            self::debug($arr,$die);
    }


    /**
     * @param $str
     * @return array
     */
    public static function explodeDays($str)
    {
        $d = explode('-',$str);
        $d = self::trimRecursive($d);
        $d = self::toIntRecursive($d);
        return ['delay_days_min'=>$d[0],'delay_days_max'=>isset($d[1]) ? $d[1]:$d[0]];
    }

    /**
     * @param $tms
     * true Mon-Fri
     * false Sat-Sun
     * @return bool
     */
    public static function isWorkingDay($tms)
    {
        $day = date('D',$tms);
        return in_array($day,['Sat','Sun']) ? false : true;
    }


    /**
     * @param array $arr
     * @return array
     */
    public static function trimRecursive(array $arr)
    {
        array_walk_recursive($arr,function(&$a){
            return trim($a);
        });
        return $arr;
    }

    /**
     * @param array $arr
     * @return array
     */
    public static function toIntRecursive(array $arr)
    {
        array_walk_recursive($arr,function(&$a){
            return intval($a);
        });
        return $arr;
    }

    /**
     * @param $time
     * @return false|string
     */
    public static function formatDate($time)
    {
        return date('Y-m-d H:i:s',$time);
    }



    /**
     * @param $time
     * @return false|string
     *
     * выводит дату в формает 21 апр 2018 по unix_timestamp()
     */
    public static function formatDateWithStringMonthShort($time)
    {
        $date = date('d.m.Y',$time);
        $parts = explode('.',$date);
        $months = self::getMonthList();
        $key = (int)$parts[1]-1;
        if (isset($months[$key]))
        {
            return (int)$parts[0] . ' '.$months[$key].' '.$parts[2];
        }
        return $date;
    }

    /*
 * Возвращает интервал
 */
    public static function getMonthList($interval=12)
    {
        if (!in_array($interval,[6,12]))
            throw new \Exception('Only 6 or 12');
        $return = 'Янв,Фев,Мар,Апр,Май,Июн,Июл,Авг,Сен,Окт,Ноя,Дек';
        $return = explode(',',$return);
        //месяц
        $month = $p = date('n');
        $monthPosition = null;

        if ($interval==6)
        {
            if ($month<=3)
            {
                $return  = array_slice($return,0,6);
                $monthPosition = $p;
            }
            elseif($month>=9)
            {
                $return  = array_slice($return,6,12);
                $monthPosition = $p - 6;
            }
            else
            {
                $return  = array_slice($return,($month-3),6);
                $monthPosition = $p - 3;
            }
        }

        return [$return,$monthPosition];
    }


    /**
     * @param $time
     * @param bool $part  day|month|year
     * @return false|string
     * выводит дату в формает 21 апреля 2018 по unix_timestamp()
     */
    public static function formatDateWithStringMonth($time=false,$part=false)
    {
        $time = $time ? $time : time();
        $date = date('d.m.Y',$time);
        $parts = explode('.',$date);
        $months = 'Января,Февраля,Марта,Апреля,Мая,Июня,Июля,Августа,Сентября,Октября,Ноября,Декабря';
        $months = explode(',',$months);
        $key = (int)$parts[1]-1;
        if (isset($months[$key]))
        {
            if ($part=='month')
                return $months[$key];
            elseif($part=='day')
                return (int)$parts[0];
            elseif($part=='year')
                return $parts[2];
            return (int)$parts[0] . ' '.$months[$key].' '.$parts[2];
        }
        return $date;
    }

    /**
     * @param $e
     * Отображает все ошибки в виде строки
     * @return string
     */
    public static function showErrors($e)
    {
        $return='';
        if (is_array($e))
        {
            array_walk_recursive($e,function($a) use(&$return){
                $return.=$a.'<br>';
            });
        }
        return $return;
    }

    /**
     * @param $time int
     * Время задано в секундах
     *
     * Функция предназначена для принудительной остановки обработки кода
     * в тех местах, где есть  вероятность получить бесконечный цикл
     * @throws \Exception
     */
    public static function loopInterruptor($time=5)
    {

        if (self::$time===null)
            self::$time = time();

        $stopMicrotime = self::$time + $time;
        $currentMicrotime = time();

        if ($stopMicrotime < $currentMicrotime)
            throw new \Exception("loop interruptor: you have an endless loop in your code");

    }



    public static function cropString($str,$length=50)
    {
        if (strlen($str)<$length)
            return $str;
        $string = substr($str,0,$length).'..';
        return $string;
    }

    /**
     * @param $dir
     */
    public static function clearDirectory($dir)
    {
        if (is_dir($dir))
        {
            foreach(scandir($dir) as $file)
            {
                if (is_file($dir.'/'.$file))
                    unlink($dir.'/'.$file);
            }

        }
    }



    public static function num2str($num,$first_upper=false,$unit_use=true) {
        $nul='ноль';
        $ten=array(
            array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
            array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
        );
        $a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
        $tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
        $hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
        $unit=array( // Units
            array('копейка' ,'копейки' ,'копеек',	 1),
            array('рубль'   ,'рубля'   ,'рублей'    ,0),
            array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
            array('миллион' ,'миллиона','миллионов' ,0),
            array('миллиард','милиарда','миллиардов',0),
        );
        //
        list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub)>0) {
            foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
                if (!intval($v)) continue;
                $uk = sizeof($unit)-$uk-1; // unit key
                $gender = $unit[$uk][3];
                list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
                else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                // units without rub & kop
                if ($unit_use && $uk>1)
                    $out[]= self::morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
            } //foreach
        }
        else $out[] = $nul;
        if ($unit_use)
            $out[] = self::morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
        if ($unit_use)
            $out[] = $kop.' '.self::morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
        if ($first_upper)
            return self::mb_ucfirst(trim(preg_replace('/ {2,}/', ' ', join(' ',$out))));
        return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
    }

    /**
     * Склоняем словоформу
     * @author runcore
     */
    public static function morph($n, $f1, $f2, $f5) {
        $n = abs(intval($n)) % 100;
        if ($n>10 && $n<20) return $f5;
        $n = $n % 10;
        if ($n>1 && $n<5) return $f2;
        if ($n==1) return $f1;
        return $f5;
    }

    /**
     * @param $name
     * @return mixed
     *
     * Заменяет ООО Свет=> Общество с ограниченное ответственностью Свет
     */
    public static function replacePrefix($name)
    {
        $prefix=[
            'ОАО'=>'Открытое акционерное общество',
            'ЗАО'=>'Закрытое акционерное общество',
            'ПАО'=>'Публичное акционерное общество',
            'НПО'=>'Непубличное акционерное общество',
            'ООО'=>'Общество с ограниченной ответственностью',
            //'ИП'=>'Индивидуальный предприниматель',
        ];
        return str_replace(array_keys($prefix),array_values($prefix),$name);
    }




    /**
     * @param $num
     * @return float
     */
    public static function toNumber($num)
    {
        return number_format($num,2,'.','');
    }

    /**
     * @param $hash
     * @return string
     */
    public static function memcacheKey($hash)
    {
        return 'search.'.$hash;
    }

    /**
     * @param $string
     * @param null $enc
     * @param null $ret
     * @return bool|mixed
     *
     * Определение кодировки
     */
    public static function compare_string_different_encodes ($s1,$s2)
    {
        $s1 = trim(mb_strtolower($s1));
        $s2= trim(mb_strtolower($s2));

        return preg_match('/'.$s1.'/',$s2);

    }


    /**
     *
     */
    public static function getCoords($address,$country='Россия'){

        $lon=null;
        $lat=null;
        if (preg_match('/'.$country.'/si',$address))
        {
            $search = $address;
        }
        else $search = $country.','.$address;

        $url = 'https://geocode-maps.yandex.ru/1.x/?geocode='.$search;

        $content = '';
        try {
            $content = file_get_contents($url);
        } catch (\Exception $e){
        }

        if (preg_match('/<pos>(.*?)<\/pos>/', $content, $point)==1) {
            $coords = str_replace(' ', ', ', trim(strip_tags($point[1])));
            $coords = explode(',', $coords);
            $lat = trim($coords[1]);
            $lon = trim($coords[0]);
        }

        return ['lat'=>$lat,'lon'=>$lon];

    }

    /**
     * @param $str
     * @param string $char
     * @return string
     */
    public static function trimStrRecursive($str,$char='_')
    {
        while(substr($str,-1) === $char)
        {
            $str = trim($str,$char);
        }
        return $str;
    }

    /**
     * @param $dataset
     * @return array
     */
    public static function getTree($dataset) {
        $tree = array();
        foreach ($dataset as $id => &$node) {
            if (!$node['parent_id']){
                $tree[$id] = &$node;
            }else{
                $dataset[$node['parent_id']]['childs'][$id] = &$node;
            }
        }
        return $tree;
    }

    /**
     * @param $text
     * @return mixed
     * Обрезает текст до нужно количество слов,
     * делает превью
     */
    public static function cropTextVersion($text,$fade=true)
    {
        $array = explode(' ',$text);
        if (count($array)>Yii::$app->params['cropWordsComment'])
        {
            $array = array_slice($array,0,Yii::$app->params['cropWordsComment']);
            $str = $fade ? implode(' ',$array).'...' : implode(' ',$array);
            return $str;
        }
        return false;

    }


    /**
     * @param $number
     * @param $root  string  Организац
     * @param $end_ro
     * @param $end_im
     * @param $end_vi
     * @return string
     */

    public static function langEnd($number, $root='организац',$end_ro='ий', $end_im='ия', $end_vi='ии') /*  компан, 0-ий, 1-ия, 2-ии  */
    {
        $number = $number % 100;
        if ($number >= 11 && $number <= 14) {
            $end =  $end_ro;
        } else {
            $rest = $number % 10;
            if ($rest == 1)
                $end = $end_im;
            elseif ($rest == 2 || $rest == 3 || $rest == 4)
                $end =  $end_vi;
            else
                $end =  $end_ro;
        }

        return $root.$end;
    }

    /**
     * @param $param
     * @param string $separator
     * @return string
     */
    public static function concatenate($param,$separator=',')
    {
        if ($param)
            return $separator.' '.$param;
        return '';
    }






}
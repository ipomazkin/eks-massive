<?php

namespace app\controllers;

use app\models\Sections;
use app\models\Settings;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\Html;
use app\components\Helper;

class SiteController extends AppController
{


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        foreach(range(1,4) as $v)
        {
            ${'section'.$v} = Yii::$app->params['sections'.$v] = Sections::getModel($v);
        }

        $settings = Yii::$app->params['settings'];

        return $this->render('index',compact('section1','section2','section3','section4','settings'));
    }


    /**
     * @return array
     */
    public function actionCall()
    {
        $model = new ContactForm(['scenario'=>ContactForm::SCENARIO_REQUEST]);

        $settings = Settings::getModel();

        if ($model->load(Yii::$app->request->post())  && $model->validate())
        {
            //шлем админу
            Yii::$app->mailer
                ->compose('mail-new-request', ['user'=>$model])
                ->setFrom(env('SENDER_EMAIL','no-reply@eks-massiv.ru'))
                ->setTo($settings->email)
                ->setSubject('Запрос с формы обратный звонок')
                ->send();
            return [
                //'size'=>'large',
                'title'=>'Запрос отправлен',
                'content'=>'Спасибо за обращение! В ближайшее время наши специалисты свяжутся с Вами',
                'footer'=>Html::button('Закрыть',[
                    'class'=>'btn btn-default btn-lg',
                    'data-dismiss'=>'modal',
                ])
            ];
        }

        return [
            'size'=>'small',
            'title'=>'Оставьте Ваши данные, и мы Вам перезвоним',
            'content'=>$this->renderAjax('request_form',compact('model')),
            'footer'=>Html::button('Отправить',[
                'class'=>'btn btn-default btn-lg',
                'type'=>"submit",
            ])
        ];
    }


}

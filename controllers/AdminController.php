<?php

namespace app\controllers;


use app\models\Sections;
use app\models\Settings;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use app\models\User;
use app\components\Helper;
use yii\web\Response;


class AdminController extends AppController
{

    public $layout = 'admin';


    /**
     * @param $action
     * @return bool
     * @throws \yii\base\ExitException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        /* @var $user \app\models\User */
        if (Yii::$app->user->isGuest && $action->id!='login'){
            Yii::$app->response->redirect(Url::to(['admin/login']), 301);
            Yii::$app->end();
        }

        return parent::beforeAction($action);
    }


    /**
     * @return array
     * Запрещаем всем на админам да и вообще пбесправным пользователям заходить
     * сюда. Только POST
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete-file' => ['POST'],
                ],
            ],
        ];

    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['admin/index']);
        }

        $user = new User(['scenario' => User::SCENARIO_LOGIN]);
        if ($user->load(Yii::$app->request->post()) && $user->validate()){

            $duration = $user->rememberMe ? 0 : 3600 * 24 * 60;

            $checkUser = User::find()
                ->where(['email'=>$user->email])
                ->one();
            if ($checkUser!==null && Yii::$app->security->validatePassword($user->password, $checkUser->password_hash)) {
                /** @var $checkUser \yii\web\IdentityInterface */
                if (Yii::$app->user->login($checkUser, $duration)) {
                    return $this->redirect(['admin/index']);
                }
            }


        }

        return  $this->render('login', [
            'model' => $user,
        ]);

    }

    /**
     *
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSettings()
    {
        $model = Settings::getModel();

        $this->view->params['breadcrumbs']=['label'=>'Общие настройки'];

        if ($model->load(Yii::$app->request->post())  && $model->save()) {
           return $this->redirect(Url::current());
        }

        return $this->render('settings', [
            'model' => $model,
        ]);
    }

    /**
     * @param $section_id
     * @throws NotFoundHttpException
     */
    public function actionSections($section_id)
    {
        if (!in_array($section_id,[1,2,3,4]))
            return $this->notFound();

        $model = Sections::getModel($section_id);

        $this->view->params['breadcrumbs']=[
            ['label'=>'Секция №'.$section_id]
        ];


        $view = 'section'.$section_id;

        if ($model->load(Yii::$app->request->post())  && $model->validate())
        {
            $model->save(false);
            Yii::$app->session->setFlash('info','Данные сохранены');
            return $this->redirect(['admin/sections','section_id'=>$section_id]);
        }

        return $this->render($view,compact('model'));
    }

    /**
     * @param $section_id
     * @param $field_id
     */
    public function actionDeleteFile($section_id,$field_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = Sections::getModel($section_id);
        if (!$model)
            return ['status'=>'error'];
        $field = 'image'.$field_id.'_src';

        try
        {
            unlink(Yii::getAlias('@images/'.$model->{$field}));

        }
        catch (\Exception $e )
        {

        }

        $model->updateAttributes([$field=>'']);
        return ['status'=>'OK'];

    }



}

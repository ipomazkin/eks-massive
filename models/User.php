<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{

    public $password = '';


    const SCENARIO_LOGIN = 3;


    public $rememberMe;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['password_hash', 'email'], 'required'],
//            [['status'], 'integer'],
//            [['username', 'password_hash', 'password_reset_token', 'email','password'], 'string', 'max' => 255],
//            [['auth_key'], 'string', 'max' => 32],
//            [['email'], 'unique'],
//            [['email'], 'email'],
//            [['password_reset_token'], 'unique'],

            //====LOGIN======
            [['email', 'password'], 'required', 'on'=>self::SCENARIO_LOGIN],
            [['password'], 'validatePassword', 'on'=>self::SCENARIO_LOGIN],
            ['rememberMe','safe', 'on'=>self::SCENARIO_LOGIN]
        ];
    }


    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'password'=>'Пароль'
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        /** @var \app\models\User $user */
        $user = User::find()->where(['email'=>$this->email])->one();
        if ($user!==null && Yii::$app->security->validatePassword($this->password,$user->password_hash)) {
            return Yii::$app->user->login($user, 3600 * 24 * 30);
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }


    /**
     * @throws \yii\base\Exception
     */
    public function generateSecret(){
        $this->updateAttributes(['password_reset_token'=>Yii::$app->security->generateRandomString()]);
    }



    public function validatePassword($attribute, $params)
    {
        /** @var \app\models\User $user */
        if (!$this->hasErrors()) {
            $user = User::find()
                ->select(['password_hash'])
                ->where(['email'=>$this->email])
                ->one();
            if ($user===null || !Yii::$app->security->validatePassword($this->password,$user->password_hash)) {
                $this->addError($attribute, 'Имя пользователя или пароль неверны');
            }
        }
    }



}

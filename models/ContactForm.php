<?php

namespace app\models;

use app\components\Helper;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;
    public $phone;

    const SCENARIO_REQUEST='request';


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            [['phone','name'],'required','on'=>self::SCENARIO_REQUEST],
            [['name','phone'],'string','on'=>self::SCENARIO_REQUEST],
            ['phone','validatePhone','on'=>self::SCENARIO_REQUEST],

            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required','on'=>self::SCENARIO_DEFAULT],
            // email has to be a valid email address
            ['email', 'email','on'=>self::SCENARIO_DEFAULT],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha','on'=>self::SCENARIO_DEFAULT],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'phone' => 'Ваш номер телефона',
            'name'=>'Ваше имя'
        ];
    }


    public function validatePhone($attr)
    {
        $str = Helper::clearArticle($this->{$attr});
        if (strlen($str)!=10)
            return $this->addError($attr,'Введите корректный формат номера телефона (10 цифр)');
    }



    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setReplyTo([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->body)
                ->send();

            return true;
        }
        return false;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $title
 * @property string $kwds
 * @property string $description
 * @property string $country
 * @property string $city
 * @property string $region
 * @property string $street
 * @property string $building
 * @property string $office
 * @property string $postcode
 * @property string $phone
 * @property string $email
 * @property string $soc_vk
 * @property string $soc_ok
 * @property string $soc_google
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['country', 'city', 'region', 'street', 'building', 'office', 'postcode', 'phone', 'email', 'soc_vk', 'soc_ok', 'soc_google','title','kwds','description'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country' => 'Country',
            'city' => 'City',
            'region' => 'Region',
            'street' => 'Street',
            'building' => 'Building',
            'office' => 'Office',
            'postcode' => 'Postcode',
            'phone' => 'Phone',
            'email' => 'Email',
            'soc_vk' => 'Soc Vk',
            'soc_ok' => 'Soc Ok',
            'soc_google' => 'Soc Google',
        ];
    }

    /**
     * @return Settings|null|static
     */
    public static function getModel()
    {
        $model = self::findOne(1);
        return $model===null ? new self(['id'=>1]) : $model;
    }
}
